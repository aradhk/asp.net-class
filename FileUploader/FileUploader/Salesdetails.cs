﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileUploader
{
    public class Salesdetails
    {
        public int Sales { get; set; }
        public int Pages { get; set; }
        public string ID { get; set; }
        public static IEnumerable<Salesdetails> getsalesdetails()
        {
            Salesdetails[] sd =
            {
 new Salesdetails { ID = "001", Pages=678, Sales = 110000},
 new Salesdetails { ID = "002", Pages=789, Sales = 60000},
 new Salesdetails { ID = "003", Pages=456, Sales = 40000},
 new Salesdetails { ID = "004", Pages=900, Sales = 80000},
 new Salesdetails { ID = "005", Pages=456, Sales = 90000},
 new Salesdetails { ID = "006", Pages=870, Sales = 50000},
 new Salesdetails { ID = "007", Pages=675, Sales = 40000},
 };
            return sd.OfType<Salesdetails>();
        }
    }
}