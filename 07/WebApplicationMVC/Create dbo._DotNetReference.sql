﻿USE [ASPDotNetStepByStep]
GO

/****** Object: Table [dbo].[ DotNetReference] Script Date: 12/1/2017 6:57:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DotNetReference] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [Title]           NVARCHAR (50) NULL,
    [AuthorFirstName] NCHAR (50)    NOT NULL,
    [AuthorLastName]  NCHAR (50)    NOT NULL,
    [Topic]           NCHAR (25)    NOT NULL,
    [Publisher]       NCHAR (50)    NOT NULL
);


