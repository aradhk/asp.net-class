﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:WebCustomControl1 runat=server></{0}:WebCustomControl1>")]
    public class WebCustomControl1 : WebControl
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string FirstName
        {
            get
            {
                String s = (String)ViewState["FirstName"];
                return ((s == null) ? "Dan" : s);
            }

            set
            {
                ViewState["FirstNamet"] = value;
            }
        }
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string LastName
        {
            get
            {
                String s = (String)ViewState["LastName"];
                return ((s == null) ? "F" : s);
            }

            set
            {
                ViewState["LastNamet"] = value;
            }
        }
        private string FullName
        {
            get
            {
                return FirstName + LastName;
            }
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            output.WriteLine("<table><tr><td>");

            output.Write("First Name----Last name");

            output.WriteLine("</tr></td>");
            output.WriteLine("<tr><td>");
            output.Write(FullName);
            output.WriteLine("</table></tr></td>");


        }
    }
}
