﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" trace="true" CodeBehind="About.aspx.cs" Inherits="WebApplication4.About" %>

<%@ Register Src="~/ViewSwitcher.ascx" TagPrefix="friendlyUrls" TagName="ViewSwitcher" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Your application description page.</h3>
    <p>Use this area to provide additional information.</p>


    <asp:Button ID="btnAbout" Text="About" runat="server" OnClick="btnAbout_Click" />
    <friendlyUrls:ViewSwitcher ID="friendlyUrls" runat="server" />

</asp:Content>
