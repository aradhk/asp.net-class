﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace WebApplication4
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //set number sessions to zero
            Application["numberOfSessions"] = 0;


        }
        void Application_Stop(object sender, EventArgs e)

        {
            Application["numberOfSessions"] = 0;
        }

            void Session_Start(object sender, EventArgs e)
        {
            int sessionsCount = (int)(Application["numberOfSessions"]);
            Application["numberOfSessions"] = sessionsCount + 1;
        }

    }
}