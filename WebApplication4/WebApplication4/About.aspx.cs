﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication4.utility;


namespace WebApplication4
{
    public partial class About : Page


    {

        string myString = string.Empty;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            // Response.Write("This is from about page PreInit");
            myString = "preInit";

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            //Response.Write("This is from about page Init");
            myString += " Init";

        }
        protected void Page_InitComplete(object sender, EventArgs e)
        {
            // Response.Write("This is from about page InitComplet");
            myString += " InitComplete";

        }
        protected void Page_PreLoad(object sender, EventArgs e)
        {
            //Response.Write("This is from about pre load page");
            myString += " Page_PreLoad";
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("This is from about page");
            myString += " Load";

        }

        protected void btnAbout_Click(object sender, EventArgs e)
            
        {
            var button = (Button)sender;
            Response.Write(button.ID + " " + button.Text);
            button.Text = "Update About";

            //store my var in session object
            //Session["myString"] = myString;
            //redirect the page to contact
            //Response.Redirect("/Contact.aspx");
           LoginInfo loginInfo = (LoginInfo) Session["loginInfo"];
            //in ziri ro be khater button pak kardam chon age oon bashe bayad az contact ejara ro shoro konam
            //Response.Write(string.Format("This is my userName: {0} This is my pass: {1} " ,loginInfo.UserName,loginInfo.Password));
        }
    }
}