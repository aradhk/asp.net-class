﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Election.aspx.cs" Inherits="WebApplication4.Election" %>
<%@ Register Src="~/footer.ascx"
 TagName="footer" TagPrefix="Tfooter" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

   
        <table style="width: 66%;">
            <tr>
                <td class="style1" colspan="3" style="text-align:center">
                    <asp:Label ID="lblmsg"
                        Text="President Election Form : Choose your president"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td class="style3">Candidate:
                </td>
                <td class="style2">
                    <asp:DropDownList ID="ddlcandidate" runat="server" Style="width: 239px">
                        <asp:ListItem Value="0">Please Choose a Candidate</asp:ListItem>
                        <asp:ListItem Value="1">M H Kabir</asp:ListItem>
                        <asp:ListItem Value="2">Steve Taylor</asp:ListItem>
                        <asp:ListItem Value="3">John Abraham</asp:ListItem>
                        <asp:ListItem Value="4">Venus Williams</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvcandidate"
                        runat="server" ControlToValidate="ddlcandidate"
                        ErrorMessage="Please choose a candidate"
                        InitialValue="0" CssClass="required">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">House:</td>
                <td class="style2">
                    <asp:RadioButtonList ID="rblhouse"
                        runat="server"
                        RepeatLayout="Flow">
                        <asp:ListItem>Red</asp:ListItem>
                        <asp:ListItem>Blue</asp:ListItem>
                        <asp:ListItem>Yellow</asp:ListItem>
                        <asp:ListItem>Green</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvhouse"
                        runat="server"
                        ControlToValidate="rblhouse"
                        ErrorMessage="Enter your house name" CssClass="required">
                    </asp:RequiredFieldValidator>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="style3">Class:</td>
                <td class="style2">
                    <asp:TextBox ID="txtclass" runat="server"></asp:TextBox>
                </td>

                



                <td>
                    <asp:RequiredFieldValidator ID="rfvClass" runat="server" ErrorMessage="Please enter a Class" ControlToValidate="txtclass" CssClass="required"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvclass"
                        runat="server" ControlToValidate="txtclass"
                        ErrorMessage="Enter your class (6 - 12)" MaximumValue="12"
                        MinimumValue="6" Type="Integer">
                    </asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">Email:</td>
                <td class="style2">
                    <asp:TextBox ID="txtemail" runat="server" Style="width: 250px">
                    </asp:TextBox>
                </td>
                

                <td>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Please Enter Email" ControlToValidate="txtemail" CssClass="required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="remail"
                        runat="server"
                        ControlToValidate="txtemail" ErrorMessage="Enter your email"
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="style3"  colspan="3">
                    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click"
                         Text="Submit" Style="text-align:center; width:140px;" />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="ValidationSummary1"
            runat="server"
            DisplayMode="BulletList"
            ShowSummary="true"
            HeaderText="Errors:" CssClass="required"/>
   
    <Tfooter:footer ID="footer1" runat="server" />

</asp:Content>
