﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication4
{
    public partial class SmartLable : System.Web.UI.UserControl
    {
        public string SmartText { get; set; }



        protected void Page_Load(object sender, EventArgs e)
        {
            //lblHello.Text =SmartText + " " + Environment.UserName;
            //lblHello.Text = SmartText + " " + Request.LogonUserIdentity.Name;
            lblHello.Text = SmartText + " " + DateTime.UtcNow;
        }
    }
}