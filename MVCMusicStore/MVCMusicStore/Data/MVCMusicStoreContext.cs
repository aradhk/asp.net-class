﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MVCMusicStore.Models
{
    public class MVCMusicStoreContext : DbContext
    {
     

        public MVCMusicStoreContext (DbContextOptions<MVCMusicStoreContext> options)
            : base(options)
        {
        }

        public DbSet<MVCMusicStore.Models.Album> Album { get; set; }

        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genre { get; set; }
    }
}
