﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using MVCMusicStore.Models;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MVCMusicStore.Controllers
{
    public class StoreController : Controller
    {
        private readonly MVCMusicStoreContext _context;
        public StoreController(MVCMusicStoreContext context)
        {
            _context = context;
        }
        // GET: /<Store>/
        public IActionResult Index()
        {
            var genres = _context.Genre.ToList();
            return View(genres);
        }

        //
        // GET: /Store/Browse
        //
        // GET: /Store/Browse?genre=Disco
        public ActionResult Browse(string genre)
        {
            // Retrieve Genre and its Associated Albums from database
            var genreModel = _context.Genre.Include("Albums")
                .Single(g => g.Name == genre);

            return View(genreModel);
        }
        //
        // GET: /Store/Details
        //
        // GET: /Store/Details/5
        public ActionResult Details(int id)
        {
            var album = _context.Albums.Find(id);

            return View(album);
        }
    }
}
