﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MidCours
{
    public partial class StudentAdonet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet ds = DataBaseManager.GetStudentInfo();


                   // Session["dsStudents"] = ds;
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
            }
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            //GridView1.DataSource = Session["dsStudents"];
            GridView1.DataSource = DataBaseManager.GetStudentInfo();
            GridView1.DataBind();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            GridView1.DataSource = DataBaseManager.GetStudentInfo();
            GridView1.DataBind();


        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Studnt s = new Studnt();
            s.Id = Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].Cells[1].Controls[0]).Text);
            s.FirstName = ((TextBox)GridView1.Rows[e.RowIndex].Cells[2].Controls[0]).Text;
            s.LastName= ((TextBox)GridView1.Rows[e.RowIndex].Cells[3].Controls[0]).Text;
            s.PhoneNumber = ((TextBox)GridView1.Rows[e.RowIndex].Cells[4].Controls[0]).Text;
            s.AddressID = Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].Cells[5].Controls[0]).Text);
            s.ClassID= Convert.ToInt32(((TextBox)GridView1.Rows[e.RowIndex].Cells[6].Controls[0]).Text);

            DataBaseManager.UpDateStudentInfo(s);
            GridView1.DataSource = DataBaseManager.GetStudentInfo();
            GridView1.DataBind();

        }

        

        

        

        
    }
}