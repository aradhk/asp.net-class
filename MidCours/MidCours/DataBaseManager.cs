﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MidCours
{
    public class Studnt
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public int AddressID { get; set; }
        public int ClassID { get; set; }
   
    }

    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public int PostalCode { get; set; }

    }

    public class ClassRoom
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
       

    }



    public class DataBaseManager
    {


        public static  DataSet GetStudentInfo()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MiddleCourseTestConnectionString"].ConnectionString);
            DataSet ds = new DataSet();
            try
            {

                conn.Open();
                SqlCommand command = new SqlCommand("[dbo].[GetStudentInfo]", conn);
                //SqlCommand command = new SqlCommand("[dbo].[UpDateStudentInfo]", conn);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sda = new SqlDataAdapter(command);

                //be jaye in do khat khat badi
                //SqlCommand cmd = new SqlCommand("selec5 * from Student", conn);
                //SqlDataAdapter sda = new SqlDataAdapter(cmd);
                //SqlDataAdapter sda = new SqlDataAdapter("select * from Student", conn);
                //SqlDataAdapter sda = new SqlDataAdapter("SELECT Student.FirstName + ' _ ' + Student.LastName AS FullName, Student.PhoneNumber," +
                // " Address.Street, Address.City, Address.PostalCode, ClassRoom.Number + ' ' +ClassRoom.Name As ClassNumberAndName FROM Student " +
                // "INNER JOIN ClassRoom ON Student.ClassID = ClassRoom.Id INNER JOIN Address ON Student.AddressID = Address.Id", conn);



                sda.Fill(ds);
            }
            catch (SqlException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return ds;

        }
    

        public static int  UpDateStudentInfo(Studnt s)
        {
            
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MiddleCourseTestConnectionString"].ConnectionString);
            int result = 0;
            try
            {
                
                conn.Open();
                
                SqlCommand command = new SqlCommand("[dbo].[ReturnStudentInfo]", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@id", SqlDbType.Int).Value = s.Id;
                command.Parameters.Add("@firstName", SqlDbType.VarChar,50).Value = s.FirstName;
                command.Parameters.Add("@lastName", SqlDbType.VarChar,50).Value = s.LastName;
                command.Parameters.Add("@phoneNumber", SqlDbType.VarChar,15).Value = s.PhoneNumber;
                command.Parameters.Add("@addressID", SqlDbType.Int).Value = s.AddressID;
                command.Parameters.Add("@classID", SqlDbType.Int).Value = s.ClassID;

                result=command.ExecuteNonQuery();


               
            }
            catch (SqlException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return result;
   

        }
    }
}