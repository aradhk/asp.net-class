﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Student.aspx.cs" Inherits="MidCours.Student" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:SqlDataSource ID="sqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MiddleCourseTestConnectionString %>" SelectCommand="SELECT Student.FirstName + ' _ ' + Student.LastName AS FullName, Student.PhoneNumber, Address.Street, Address.City, Address.PostalCode, ClassRoom.Number+' '+ ClassRoom.Name As ClassNumberAndName FROM Student INNER JOIN ClassRoom ON Student.ClassID = ClassRoom.Id INNER JOIN Address ON Student.AddressID = Address.Id" ProviderName="System.Data.SqlClient"></asp:SqlDataSource>
        </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="sqlDataSource1" Height="195px" Width="713px" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None">
            <Columns>
                <asp:BoundField DataField="FullName" HeaderText="        FullName         " ReadOnly="True" SortExpression="FullName" />
                <asp:BoundField DataField="PhoneNumber" HeaderText="StudentPhone#" SortExpression="PhoneNumber" />
                <asp:BoundField DataField="Street" HeaderText="AddressStreet" SortExpression="Street" />
                <asp:BoundField DataField="City" HeaderText="AddressCity" SortExpression="City" />
                <asp:BoundField DataField="PostalCode" HeaderText="AddressPostalCode" SortExpression="PostalCode" />
                <asp:BoundField DataField="ClassNumberAndName" HeaderText="ClassNumberAndName" SortExpression="ClassNumberAndName" ReadOnly="True" />
            </Columns>
            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#594B9C" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#33276A" />
        </asp:GridView>
        <asp:TextBox ID="txtID" runat="server"></asp:TextBox>
    </form>
</body>
</html>
