﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Tests.WebForm1"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:TreeView ID="TreeView1" runat="server" ImageSet="XPFileExplorer" NodeIndent="15" OnSelectedNodeChanged="TreeView1_SelectedNodeChanged1">
            <HoverNodeStyle Font-Underline="True" ForeColor="#6666AA" />
            <Nodes>
                <asp:TreeNode Text="Science Club" Value="Science Club">
                    <asp:TreeNode Text="picture Galery" Value="picture Galery"></asp:TreeNode>
                    <asp:TreeNode Text="Home" Value="Home">
                        <asp:TreeNode Text="History" Value="History"></asp:TreeNode>
                        <asp:TreeNode Text="About Us" Value="About Us"></asp:TreeNode>
                        <asp:TreeNode Text="Contact Us" Value="Contact Us"></asp:TreeNode>
                    </asp:TreeNode>
                    <asp:TreeNode Text="Product" Value="Product"></asp:TreeNode>
                    <asp:TreeNode Text="Research" Value="Research"></asp:TreeNode>
                    <asp:TreeNode Text="Board Meember" Value="Board Meember">
                        <asp:TreeNode Text="M H Kabir" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="Akhilesh Agarwal" Value="Akhilesh Agarwal"></asp:TreeNode>
                        <asp:TreeNode Text="Aditya Dey" Value="Aditya Dey"></asp:TreeNode>
                        <asp:TreeNode Text="Sumit Sahay" Value="Sumit Sahay"></asp:TreeNode>
                    </asp:TreeNode>
                </asp:TreeNode>
            </Nodes>
            <NodeStyle Font-Names="Tahoma" Font-Size="8pt" ForeColor="Black" HorizontalPadding="2px" NodeSpacing="0px" VerticalPadding="2px" />
            <ParentNodeStyle Font-Bold="False" />
            <SelectedNodeStyle BackColor="#B5B5B5" Font-Underline="False" HorizontalPadding="0px" VerticalPadding="0px" />
        </asp:TreeView>
        <asp:Label ID="LblNode" runat="server" Text="Label"></asp:Label>
        <br />
        <asp:TextBox ID="txtNode" runat="server" Height="123px" TextMode="MultiLine" Width="192px"></asp:TextBox>
        <br />
    </form>
</body>
</html>
