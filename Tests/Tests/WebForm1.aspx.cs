﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Tests
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtNode.Text = string.Empty;

        }

       

        protected void TreeView1_SelectedNodeChanged1(object sender, EventArgs e)
        {
            //String textMessage = string.Empty;
            LblNode.Text = TreeView1.SelectedValue;
            //==LblNode.Text = ((TreeView)sender).SelectedValue;
            //set the text value with the name of children nodes
            if (TreeView1.SelectedNode.ChildNodes != null)
            {
                foreach (TreeNode node in TreeView1.SelectedNode.ChildNodes)
                {
                    txtNode.Text += node.Text + "\n";
                    if (node.ChildNodes != null)
                    {
                        foreach(TreeNode innerNode in node.ChildNodes)
                        {
                            txtNode.Text += "    >" + innerNode.Text + "\n";
                        } 
                    }

                }
            }

        }
    }
}