﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MvcMovie2.Controllers
{
    public class HelloWorldController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            //return View("Views/Home/Contact.cshtml");
            return View();


        }
        //public string Welcome(string name)
        //{
        //    return string.Format("Welcome {0}", name);
        //}
        //public string Welcome(string id)
        //{
        //    return string.Format("Welcome {0}", id);
        //}
        public string Welcome(string id,string Name)
        {
            return string.Format("Welcome {0} to the class {1}",Name, id);
        }
    }
}
