﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogForm.aspx.cs" Inherits="LoginPage.LogForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <p>User Name:<asp:TextBox ID="tbUser" runat="server"></asp:TextBox></p>
        
            <p>Password:<asp:TextBox ID="tbPass" runat="server" TextMode="Password"></asp:TextBox></p>
            <p>
                <asp:Button ID="btnOK" runat="server" Text="OK" OnClick="btnOK_Click" Width="123px" />
            </p>
        </div>
    </form>
</body>
</html>
